<?php

/**
 * documents for quote
 */
$app['docs'] = [
    "1" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/1 ACORD125 FILLABLE FORM.pdf",
    "2" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/2 ACORD126 FILLABLE FORM.pdf",
    "3" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/3 ACORD140 FILLABLE FORM.pdf",
    "4" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/4 ACORD130 FILLABLE FORM.pdf",
    "5" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/5 ACORD131 Umbrella Supplemental Application.pdf",
    "6" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/6 ACCIRD127.pdf",
    "7" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/7 BOP20App20 20160.pdf",
    "8" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/8 Acord-80-201309-Homeowner-Application.pdf",
    "9" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/9 Acord-84-201309-Dwelling-Fire-Application.pdf",
    "10" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/10 Rentals Acord App.pdf",
    "11" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/11 Professional Liability Supp.pdf",
    "12" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/numbers/12 Flood Supp.pdf",
    "A" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/A.pdf",
    "B" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/B.pdf",
    "C" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/C.pdf",
    "C1" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/C1.pdf",
    "D" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/D.pdf",
    "D1" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/D1.pdf",
    "E" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/E.pdf",
    "F" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/F.pdf",
    "G" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/G.pdf",
    "H" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/H.pdf",
    "I" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/I.pdf",
    "J" => 'http://' . $_SERVER['HTTP_HOST'] . "/docs/letters/J.pdf"
];

/**
 * Email configuration
 */

// for localhost
if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
    $app['frontEndUrl'] = 'http://localhost:3000/';

    $app['swiftmailer.options'] = array(
        'host' => 'smtpout.secureserver.net',
        'port' => '25',
        'username' => 'insurance@simonagency.com',
        'password' => '123sms456!',
        'encryption' => null,
        'auth_mode' => null
    );

    $app['brokerAppoinmentRecipients'] = array(
        'kostin@softgrad.com', 
        'alexp@softgrad.com'
    );

    $app['floatroot'] = ROOT;

    /**
     * Storage config
     */
    $app['data.storage.driver'] = 'mysql';
    $app['data.storage.config'] = [
        'postgres' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'project_simon',
            'user'      => 'local',
            'password'  => 'local'
        ],
        'postgres_json' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'simon',
            'user'      => 'local',
            'password'  => 'local'
        ],
        'mysql' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'simon',
            'user'      => 'local',
            'password'  => '1qaz@WSX'
        ]
    ];
    
    $app['agencyEmailTo'] = array('kostin@softgrad.com');

    $app['additionalRecipients'] = array('kostin@softgrad.com', 'alexp@softgrad.com');
    $app['simonRecipientsHomeowners'] = array('kostin@softgrad.com');
    $app['simonRecipientsNONHomeowners'] = array('alexp@softgrad.com');

    $app['contactUsTo'] = array('kostin@softgrad.com');
    $app['contactUsCc'] = array('alexp@softgrad.com');
}
// for prod
else {
    $app['frontEndUrl'] = 'https://simonagency.com/';

    $app['swiftmailer.options'] = array(
        'host' => 'relay-hosting.secureserver.net',
        'port' => '25',
        'username' => 'insurance@simonagency.com',
        'password' => '123sms456!',
        'encryption' => null,
        'auth_mode' => null
    );

    $app['brokerAppoinmentRecipients'] = array(
        'simonagency@gmail.com',
        'ba@simonagency.com',
        'shawn@simonagencyny.com',
        'icampusano@simonagencyny.com',
        'paula@simonagencyny.com',
        'brian@simonagencyny.com',
        'bob@simonagencyny.com',
        'wselvin@simonagencyny.com'
    );

    $app['floatroot'] = ROOT . "/../"; 

    /**
     * Storage config
     */
    $app['data.storage.driver'] = 'mysql';
    $app['data.storage.config'] = [
        'postgres' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'project_simon',
            'user'      => 'local',
            'password'  => 'local'
        ],
        'postgres_json' => [
            'host'      => '127.0.0.1',
            'dbname'    => 'simon',
            'user'      => 'local',
            'password'  => 'local'
        ],
        'mysql' => [
            /**
            * Old GD Server creds
            'host'      => 'simondirectapi.db.9045292.3fb.hostedresource.net',
            */
            'host'      => 'localhost',
            'dbname'    => 'simondirectapi',
            'user'      => 'simondirectapi',
            'password'  => 's1qaz@WSX'
        ]
    ];  

    $app['agencyEmailTo'] = array('jtruitt@simonagencyny.com');

    $app['additionalRecipients'] = array('shawn@simonagencyny.com', 'simonagency@gmail.com', 'quotes@simonagency.com');
    $app['simonRecipientsHomeowners'] = array('rsantis@simonagencyny.com', 'rramsaran@simonagencyny.com', 'jennifer@simonagencyny.com');
    $app['simonRecipientsNONHomeowners'] = array('jtruitt@simonagencyny.com', 'ikessler@simonagencyny.com', 'sgleason@simonagencyny.com', 'sbhola@simonagencyny.com');

    $app['contactUsTo'] = array('shawn@simonagencyny.com');
    $app['contactUsCc'] = array('simonagency@gmail.com', 'quotes@simonagency.com');
}

$app['tz'] = [
    'Pacific' => 'PT7H',
    'Mountain' => 'PT6H',
    'Central' => 'PT5H',
    'Eastern' => 'PT4H'
];

/**
 * File configuration
 */

$app['files.uploads'] = 'uploads';

/**
 * Monolog configuration
 */
$app['monolog.level'] = Monolog\Logger::DEBUG;
$app['monolog.logfile'] = '../app/logs/application.log';


// TODO: fill config on server
/**
 * Hiscox API configuration
 */
$app['hiscox.api.config'] = [
    'api_url'           => 'https://sdbx.hiscox.com',
    'consumer_key'      => '14m5QnQhXAJ6BRYG7edrq02GF5NhXlAQ',
    'consumer_secret'   => '4MQ0C469kk2Tc4Yq'
];

$app['hiscox.apigee.config'] = [
    'api_url'           => 'https://hiscox-us-sdbx.apigee.net',
    'consumer_key'      => '14m5QnQhXAJ6BRYG7edrq02GF5NhXlAQ',
    'consumer_secret'   => '4MQ0C469kk2Tc4Yq'
];

/**
 * Recaptcha config
 */
$app['recaptcha.secret'] = '6Lcnz0wUAAAAAALqrWnQF77raRqWamItMuOs3lCu';

/**
 * Broker Appointment config
 */
$app['brokerAppointment.maxSizeFile'] = 10485760; //bytes