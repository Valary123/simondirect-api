<?php

//Api routes
$app->get('{project}api/teams/', 'SimonApi\\Controller\\ApiController::getTeamsAction')
    ->assert('project', '.*');
$app->get('{project}api/cobsByState/{state}', 'SimonApi\\Controller\\ApiController::getCobsByStateAction')
    ->assert('project', '.*');
$app->get('{project}api/cobs', 'SimonApi\\Controller\\ApiController::getCobsAction')
    ->assert('project', '.*');
$app->get('{project}api/states', 'SimonApi\\Controller\\ApiController::getStatesAction')
    ->assert('project', '.*');
$app->get('{project}api/eligible/{state}/{cob}', 'SimonApi\\Controller\\ApiController::getEligibleProductsAction')
    ->assert('project', '.*');
$app->get('{project}api/eligible/tree', 'SimonApi\\Controller\\ApiController::getTreeAction')
    ->assert('project', '.*');
$app->get('{project}api/all', 'SimonApi\\Controller\\ApiController::getAllAction')
    ->assert('project', '.*');    
$app->get('{project}api/updateCobsWithState', 'SimonApi\\Controller\\ApiController::updateCobsWithState')
    ->assert('project', '.*');
$app->post('{project}api/sendUrlToClient', 'SimonApi\\Controller\\ApiController::sendUrlToClient')
    ->assert('project', '.*');
$app->post('{project}api/completeByYourself', 'SimonApi\\Controller\\ApiController::completeByYourself')
    ->assert('project', '.*');
$app->post('{project}api/brokerAppointment', 'SimonApi\\Controller\\ApiController::brokerAppointment')
    ->assert('project', '.*');
$app->post('{project}api/forgetBrokerId', 'SimonApi\\Controller\\ApiController::forgetBrokerId')
    ->assert('project', '.*');
$app->post('{project}api/sendContactUs/', 'SimonApi\\Controller\\ApiController::sendContactUs')
    ->assert('project', '.*');



//Error handler
$app->error(function (\Exception $e, \Symfony\Component\HttpFoundation\Request $request, $code) {
    $response = [
        'error'     => true,
        'message'   => ''
    ];
    switch ($code) {
        case 404:
            $response['message'] = 'Unknown resource requested';
            break;
        case 400:
            $response['message'] = 'Bad request';
            break;
        default:
            $response['message'] = 'Internal server error';
    }

    return new \Symfony\Component\HttpFoundation\JsonResponse($response);
});
