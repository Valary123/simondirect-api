<?php

// Logger
$app->register(
    new \Silex\Provider\MonologServiceProvider(),
        [
            'monolog.level'     => $app['monolog.level'],
            'monolog.logfile'   => $app['monolog.logfile']
        ]
);

//Data storage service
$app->register(new \SimonApi\Provider\DataStorageProvider());

// swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider(), [
    'swiftmailer.options'           => $app['swiftmailer.options'],
    'swiftmailer.use_spool'         => false
]);