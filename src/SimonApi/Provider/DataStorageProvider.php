<?php

namespace SimonApi\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use SimonApi\Service\DataStorage\Adapter\AdapterFactory;
use SimonApi\Service\DataStorage\DataStorage;

/**
 * Provider for data storage as service
 *
 * Class DataStorageProvider
 * @package RSnake\Provider
 */
class DataStorageProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['data.storage.service'] = function () use ($app) {
            $adapter = AdapterFactory::getAdapter($app['data.storage.driver'], $app['data.storage.config']);
            return new DataStorage($adapter);
        };
    }
}