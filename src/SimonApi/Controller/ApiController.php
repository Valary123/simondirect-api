<?php

namespace SimonApi\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swift_Attachment;

class ApiController
{
    const STATES_FIELD_NAME = 'states';
    const COBS_FIELD_NAME = 'cob';
    const ELIGIBLE_PRODUCTS_FIELD_NAME = 'eligible_products';

    /**
     * Action for states
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getStatesAction(Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getStates());
    }

    /**
     * Action for cobs
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getCobsAction(Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getCobs());
    }

    /**
     * Action for cobs with state
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getCobsByStateAction(Application $app, $state)
    {        
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getCobsByState($state));
    }

    public function getTeamsAction(Application $app)
    {        
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getTeams());
    }


    /**
     * Action for data tree
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getTreeAction(Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getEligibleProductLinkList());
    }

    /**
     * Action for eligible products
     *
     * @param Application $app
     * @param $state
     * @param $cob
     * @return JsonResponse
     */
    public function getEligibleProductsAction(Application $app, $state, $cob) {
        $dataStorage = $app['data.storage.service'];
        return new JsonResponse($dataStorage->getEligibleProductsByStateAndCob($state, $cob));
    }

    /**
     * Action for all data
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getAllAction(Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        $result = [];
        $states = $dataStorage->getStates();
        $cobs = $dataStorage->getCobs();
        $eligibleProducts = $dataStorage->getEligibleProducts();
        if(!empty($states)) {
            $result[self::STATES_FIELD_NAME] = $states;
        }
        if(!empty($cobs)) {
            $result[self::COBS_FIELD_NAME] = $cobs;
        }
        if(!empty($eligibleProducts)) {
            $result[self::ELIGIBLE_PRODUCTS_FIELD_NAME] = $eligibleProducts;
        }
        return new JsonResponse($result);
    }

    /**
     * Send url to client
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendUrlToClient(Request $request, Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        $result = '';
        $brokerIdOrEmail = $request->get('brokerIdOrEmail');
        $clientEmail = $request->get('clientEmail');
        $chosenCOB =  $request->get('chosenCOB');
        $chosenState = $request->get('chosenState');
        $businessName = $request->get('businessName');
        $chosenHiscoxCOB = $request->get('chosenHiscoxCOB');

        if (is_numeric($brokerIdOrEmail)) {
            $broker = $dataStorage->getBrokerById($brokerIdOrEmail);
        }
        else {
            $broker = $dataStorage->getBrokerByEmail($brokerIdOrEmail);
        }

        if ( $broker ) {

            // form url

            $url = $app['frontEndUrl'] . 'hiscox.html';
            $params = 
                '?restoreforms=false' .
                '&agentName=' . urlencode($broker['name']) .
                '&agencyName=' . urlencode($broker['agencyName']) .
                '&agentId=' . urlencode($broker['id']) .
                '&phoneNo=' . urlencode($broker['phone']) .
                '&agentEmail=' . urlencode($broker['email']) .
                '&businessName=' . urlencode($businessName) .
                '&state=' . urlencode($chosenState) .
                '&cob=' . urlencode($chosenCOB);
            
            // sending an email
            $emailBody = '';
            $emailBody .= '<p>Hello,</p>';
            $emailBody .= '<p>At the request of your insurance broker, we have provided you with the below link to process an online quote for the Hiscox portfolio of insurance products.</p>';
            $emailBody .= '<p>For your convenience, the entire application process can be submitted online via a smartphone, tablet or desktop device in minutes. It’s easy. It’s fast. <b style="font-style: italic;">It’s cost efficient!</b></p>';

            $emailBody .= '<p style="padding-left:40px;"><b>Step 1:</b> Please click the link below to get started</p>';
            $emailBody .= '<p style="padding-left:80px;"><a href="' . $url . $params . '">' . $url . $params . '</a></p>';
            $emailBody .= '<p style="padding-left:40px;"><b>Step 2:</b> Confirm that you are a Human</p>';
            $emailBody .= '<p style="padding-left:40px;"><b>Step 3:</b> Complete the Application</p>';

            $emailBody .= '<p>If you should have any questions, please feel free to give your broker(s) a call. We have cc’d them in this correspondence.</p>';
            $emailBody .= '<p>Enjoy your day!</p>';
            $emailBody .= '<p>Best,<br>';
            $emailBody .= 'Service Support</p>';

            $result = $emailBody;

            $emailTo = array($clientEmail);
            array_push($emailTo, $broker['email']);

            // send an email
            $message = \Swift_Message::newInstance()
                ->setSubject('Insurance Application From Your Broker')
                ->setFrom(array('insurance@simonagency.com'))
                ->setTo(array_merge($emailTo, $app['agencyEmailTo']))
                ->setBcc($app['additionalRecipients'])
                ->setBody($emailBody)
                ->addPart(
                    $emailBody,
                    'text/html'
                );
            $app['mailer']->send($message);

            $result = 'email sended';
        }
        else {
            $result = 'no broker';
        }
        
        return new JsonResponse($result);
    }

    /**
     * Complete by yourself section
     *
     * @param Request $request
     * @param Application $app
     * @return JsonResponse
     */
    public function completeByYourself(Request $request, Application $app)
    {
        $dataStorage = $app['data.storage.service'];
        $result = '';
        $brokerIdOrEmail = $request->get('brokerIdOrEmail');
        $chosenCOB =  $request->get('chosenCOB');
        $chosenState = $request->get('chosenState');
        $chosenQuoteType = $request->get('chosenQuoteType');
        $chosenProducts = $request->get('chosenProducts');

        if (is_numeric($brokerIdOrEmail)) {
            $broker = $dataStorage->getBrokerById($brokerIdOrEmail);
        }
        else {
            $broker = $dataStorage->getBrokerByEmail($brokerIdOrEmail);
        }
        if ( $broker ) {
            // instant quoute
            if ( 2 == $chosenQuoteType ) {
                $result = $broker;
            }
            // shop it around
            else if ( 3 == $chosenQuoteType ) {
                // adding doc links to email body, according list
                $docs = $request->get('docs');
                $emailBody = '';
                if ( !empty($docs) ) {

                    $products = join(', ', $chosenProducts);

                    $emailBody .= $broker['name'].',<br /><br />';

                    $emailBody .= 'Thank you for requesting a quote from the Simon Agency.  We are going to help you shop the case around to find the best price and benefit provisions for your client.<br /><br />';

                    $emailBody .= '<b>Industry:</b>  '.$chosenCOB.'<br />';
                    $emailBody .= '<b>State:</b>  '.$chosenState.'<br />';
                    $emailBody .= '<b>Products:</b>  '.$products.'<br /><br />';

                    $emailBody .= 'Instructions:<br /><br />';

                    $emailBody .= '1 – <b>Write this number down:  516-214-8618</b><br /><br />';

                    $emailBody .= 'If you should have any questions, please feel free to reach out to us.<br />';
                    $emailBody .= 'NOTE:  If you would like access to our online links to quote cases yourself, send Issa an email and she will set you up right away with our digital carriers.<br /><br />';

                    $emailBody .= '2 – <b>Do you need an Acord and or Supplement?</b><br /><br />';

                    $emailBody .= 'The applications needed for us to quote this risk are provided below. If you <i><b>already</b></i> have Acords and Supplements ready to send, please email them to your underwriter and we will start quoting ASAP.';

                    $emailBody .= '<ul>';
                    foreach ($docs as $docKey) {
                        if (array_key_exists($docKey, $app['docs'])) {
                            $emailBody .= '<li><a href="' . $app['docs'][$docKey] . '">' . $app['docs'][$docKey] . '</a></li>';
                        }
                    }
                    $emailBody .= '</ul><br />';

                    $emailBody .= '3 – <b>Email or Fax us the completed documents</b> (we use these for multiple companies)';
                    $emailBody .= '<ul>';
                    $emailBody .= '<li>Email:  <a href="mailto:jtruitt@simonagency.com">jtruitt@simonagency.com</a></li>';
                    $emailBody .= '<li>Fax:  516-280-9247</li>';
                    $emailBody .= '<li>Call:  516-214-8618 – Questions</li>';
                    $emailBody .= '<li>Online Quoting Registration: Please email Issa (icampusano@simonagency.com)</li>';
                    $emailBody .= '</ul><br />';
                    $emailBody .= 'Thank you for the opportunity to be of service.  We will get back to you shortly!<br /><br />';
                    $emailBody .= 'Sincerely,<br />';
                    $emailBody .= 'SimonAgency.com<br />';
                    $emailBody .= 'Underwriting<br />';

                    $emailTo = array($broker['email']);
                    $productsSearch = ['Homeowners - Owner Occupied', 'Dwelling - Fire Tenant Occupied', 'Renters'];
                    $productsExist = array_intersect ($chosenProducts, $productsSearch);
                    $agencyEmailTo = $app['agencyEmailTo'];
                    if($chosenCOB == 'Homeowners') {
                        if (count($productsExist)>0) {
                            $agencyEmailTo = $app['simonRecipientsHomeowners'];
                        }
                    }
                    else {
                        if (count($productsExist)==0) {
                            $agencyEmailTo = $app['simonRecipientsNONHomeowners'];
                        }
                    }

                    // send an email
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Simon Agency:  Processing Your New Quote')
                        ->setFrom(array('insurance@simonagency.com'))
                        ->setTo(array_merge($emailTo, $agencyEmailTo))
                        ->setBcc($app['additionalRecipients'])
                        ->setBody($emailBody)
                        ->addPart(
                            $emailBody,
                            'text/html'
                        );

                    $app['mailer']->send($message);
                    $result = 'email sended';
                }
            }
        }
        else {
            $result = 'no broker';
        }

        return new JsonResponse($result);
    }

    /**
     * Broker appointment data
     *
     * @param Request $request
     * @param Application $app
     * @return JsonResponse
     */
    public function brokerAppointment(Request $request, Application $app)
    {
        $result = [];
        $valid = true;

        $f_name = $request->get('f_name');
        $l_name = $request->get('l_name');
        $c_name = $request->get('c_name');
        $email = $request->get('email');
        $tel = $request->get('tel');
        $m_email = $request->get('m_email');
        $city = $request->get('city');
        $business = $request->get('business');
        $sheduledate = $request->get('sheduledate');
        $sheduletimezone = $request->get('sheduletimezone');
        $sheduletime = $request->get('sheduletime');

        // handle errors like no file, or emailing mulfunction
        if ( filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {

            $result['message'] = 'email error';
            $valid = false;
        }
        if ( !is_uploaded_file($_FILES['f_license']['tmp_name']) || 
             !is_uploaded_file($_FILES['f_eo']['tmp_name']) || 
             !is_uploaded_file($_FILES['f_w9']['tmp_name']) || 
             !is_uploaded_file($_FILES['f_agreement']['tmp_name']) ) {

            $result['message'] = 'file error';
            $valid = false;
        }

        // check file sizes (frontend and backend)
        if ( $_FILES['f_license']['size'] > $app['brokerAppointment.maxSizeFile'] || 
             $_FILES['f_eo']['size'] > $app['brokerAppointment.maxSizeFile'] || 
             $_FILES['f_w9']['size'] > $app['brokerAppointment.maxSizeFile'] || 
             $_FILES['f_agreement']['size'] > $app['brokerAppointment.maxSizeFile'] ) {

            $result['message'] = 'file size error';
            $valid = false;
        }

        // TODO move magic constants

        // check captcha
        $recaptcha_response = $request->get('g-recaptcha-response');
        $rsp = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $app['recaptcha.secret'] . '&response=' . $recaptcha_response . '&remoteip=' . $_SERVER['REMOTE_ADDR']);
        $arr = json_decode($rsp, TRUE);
        if(!$arr['success']){

            $result['message'] = 'captcha error';
            $valid = false;
        }

        // handle error parsing date
        $valid_date = date_parse($sheduledate . ' ' . $sheduletime);
        if ( $valid_date === false && time() < strtotime($sheduledate . ' ' . $sheduletime) ) {

            $result['message'] = 'date error';
            $valid = false;
        }

        if ( $valid ) {
            // create base if not exists
            if (!file_exists($app['floatroot'] . $app['files.uploads']) ) {
                mkdir($app['floatroot'] . $app['files.uploads'], 0777, true);
            }

            // create client upload folder
            $c_folder = preg_replace('/\s+/', '', $c_name) . '_' . time();
            $clientUploadFolder = $app['files.uploads'] . '/' . $c_folder;
            if (!file_exists($app['floatroot'] . $clientUploadFolder) ) {
                mkdir($app['floatroot'] . $clientUploadFolder, 0777, true);
            }

            // save files
            $uploadedFiles = [];
            $license_file = '';
            if ( isset($_FILES['f_license']) ) {

                $license_file = basename($_FILES['f_license']['tmp_name']) . '.' . pathinfo($_FILES['f_license']['name'], PATHINFO_EXTENSION);
                $fPath = 
                    $clientUploadFolder 
                    . '/' . $license_file;
                if (move_uploaded_file($_FILES['f_license']['tmp_name'], $app['floatroot'] . $fPath)) {
                    $uploadedFiles['Licence'] = [
                        'name' => $_FILES['f_license']['name'],
                        'path' => $fPath
                    ];
                }
            }
            $eo_file = '';
            if ( isset($_FILES['f_eo']) ) {

                $eo_file = basename($_FILES['f_eo']['tmp_name']) . '.' . pathinfo($_FILES['f_eo']['name'], PATHINFO_EXTENSION);
                $fPath = 
                    $clientUploadFolder 
                    . '/' . $eo_file;
                if (move_uploaded_file($_FILES['f_eo']['tmp_name'], $app['floatroot'] . $fPath)) {
                    $uploadedFiles['EO'] = [
                        'name' => $_FILES['f_eo']['name'],
                        'path' => $fPath
                    ];
                }            
            }
            $w9_file = '';
            if ( isset($_FILES['f_w9']) ) {

                $w9_file = basename($_FILES['f_w9']['tmp_name']) . '.' . pathinfo($_FILES['f_w9']['name'], PATHINFO_EXTENSION);
                $fPath = 
                    $clientUploadFolder 
                    . '/' . $w9_file;
                if (move_uploaded_file($_FILES['f_w9']['tmp_name'], $app['floatroot'] . $fPath)) {
                    $uploadedFiles['W9'] = [
                        'name' => $_FILES['f_w9']['name'],
                        'path' => $fPath
                    ];
                }            
            }
            $agreement_file = '';
            if ( isset($_FILES['f_agreement']) ) {  

                $agreement_file = basename($_FILES['f_agreement']['tmp_name']) . '.' . pathinfo($_FILES['f_agreement']['name'], PATHINFO_EXTENSION);
                $fPath = 
                    $clientUploadFolder 
                    . '/' . $agreement_file;
                if (move_uploaded_file($_FILES['f_agreement']['tmp_name'], $app['floatroot'] . $fPath)) {
                    $uploadedFiles['Agreement'] = [
                        'name' => $_FILES['f_agreement']['name'],
                        'path' => $fPath
                    ];
                }            
            }

            // ISC file generation
            require_once(ROOT . 'src/SimonApi/Service/ISC/ISC.php');

            $dateForISC = \DateTime::createFromFormat('M d Y H:i A', $sheduledate . ' ' . $sheduletime);
            $dateForISC = $dateForISC->add(new \DateInterval($app['tz'][$sheduletimezone]));

            $properties = array(
              'summary' => 'Your SimonAgency Broker Appointment Call',
              'description' => 'Your SimonAgency Broker Appointment Call',
              'dtstart' => $dateForISC->format('Ymd\THis\Z'),
              'dtend' => $dateForISC->format('Ymd\THis\Z'),
            );

            $ics = new ICS($properties);
            $ics_file_contents = $ics->to_string();
            $icsFilePath = $app['floatroot'] . $clientUploadFolder . '//iCalendar.isc';
            file_put_contents($icsFilePath, $ics_file_contents);

            // save to DB   
            $dataStorage = $app['data.storage.service'];
            $brokerAppointment = [];
            $brokerAppointment['first_name'] = $f_name;
            $brokerAppointment['last_name'] = $l_name;
            $brokerAppointment['company_name'] = $c_name;
            $brokerAppointment['email'] = $email;
            $brokerAppointment['telephone'] = $tel;
            $brokerAppointment['mailing_address'] = $m_email;
            $brokerAppointment['city'] = $city;
            $brokerAppointment['business_interested'] = $business;
            $brokerAppointment['schedule_date'] = $dateForISC->format('Y-m-d H:i');
            $brokerAppointment['schedule_tz'] = $sheduletimezone;
            $brokerAppointment['license_file'] = $license_file;
            $brokerAppointment['license_name'] = $uploadedFiles['Licence']['name'];
            $brokerAppointment['eo_file'] = $eo_file;
            $brokerAppointment['eo_name'] = $uploadedFiles['EO']['name'];
            $brokerAppointment['w9_file'] = $w9_file;
            $brokerAppointment['w9_name'] = $uploadedFiles['W9']['name'];
            $brokerAppointment['agreement_file'] = $agreement_file;
            $brokerAppointment['agreement_name'] = $uploadedFiles['Agreement']['name'];
            $brokerAppointment['company_folder'] = $c_folder;
            $dataStorage->updateBrokerAppointment($brokerAppointment);

            // composing email
            // fields
            $emailBody = 'Thank you for your interest in becoming appointed with the Simon Agency. The below listed information serves as confirmation that we received your form submission. Please <b>mark your calendar</b>, we’ll give you a call on the date and time you picked to answer your questions and introduce our resources.<br><br>';
            $emailBody .= 'Submitted Form Answers:';
            $emailBody .= '<ul>';
            $emailBody .= '<li><b>First Name:</b> ' . $f_name . '</li>';
            $emailBody .= '<li><b>Last Name:</b> ' . $l_name . '</li>';
            $emailBody .= '<li><b>Company Name:</b> ' . $c_name . '</li>';
            $emailBody .= '<li><b>Email Address:</b> ' . $email . '</li>';
            $emailBody .= '<li><b>Telephone:</b> ' . $tel . '</li>';
            $emailBody .= '<li><b>Mailing Address:</b> ' . $m_email . '</li>';
            $emailBody .= '<li><b>City, State, Zip Code:</b> ' . $city . '</li>';
            $emailBody .= '<li><b>What line of business are you most interested in writing:</b> ' . $business . '</li>';
            $emailBody .= '<li style="margin-top:15px;"><b>Scheduled Date:</b> ' . $sheduledate . '</li>';
            $emailBody .= '<li><b>Time Zone:</b> ' . $sheduletimezone . '</li>';
            $emailBody .= '<li><b>Scheduled Time:</b> ' . $sheduletime . '</li>';
            $emailBody .= '</ul><br>';
            // files
            $emailBody .= 'Submitted Attachments:';
            $emailBody .= '<ul>';
            $emailBody .= '<li><a href="' . 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uploadedFiles['Licence']['path'] . '">Copy of License: ' . $uploadedFiles['Licence']['name'] . '</a></li>';
            $emailBody .= '<li><a href="' . 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uploadedFiles['EO']['path'] . '">Copy of E&O: ' . $uploadedFiles['EO']['name'] . '</a></li>';
            $emailBody .= '<li><a href="' . 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uploadedFiles['W9']['path'] . '">W-9: ' . $uploadedFiles['W9']['name'] . '</a></li>';
            $emailBody .= '<li><a href="' . 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uploadedFiles['Agreement']['path'] . '">Broker Agreement : ' . $uploadedFiles['Agreement']['name'] . '</a></li>';
            $emailBody .= '</ul><br>';
            $emailBody .= 'Meantime, if you should have any questions or concerns, feel free to reach out.<br><br>';
            $emailBody .= 'Speak soon,<br>';
            $emailBody .= 'Simon Agency<br>';
            $emailBody .= '516-214-8618';

            // send an email to Simon and Co
            $messageToSimon = \Swift_Message::newInstance()
                ->setSubject('Simon Agency: Scheduled Call Confirmation')
                ->setFrom(['insurance@simonagency.com'])
                ->setTo($app['brokerAppoinmentRecipients'])
                ->setBody($emailBody)
                ->addPart(
                    $emailBody,
                    'text/html'
                );
            $emailingSimonResult = $app['mailer']->send($messageToSimon);

            // send an email to Client
            $messageToClient = \Swift_Message::newInstance()
                ->setSubject('Simon Agency: Scheduled Call Confirmation')
                ->setFrom(['insurance@simonagency.com'])
                ->setTo([$email])
                ->setBody($emailBody)
                ->addPart(
                    $emailBody,
                    'text/html'
                )
                ->attach(
                    Swift_Attachment::fromPath($icsFilePath)->setFilename('iCalendar.ics')
                );
            $emailingClientResult = $app['mailer']->send($messageToClient);

            if ( 
                $emailingSimonResult 
                && $emailingClientResult 
            ) {
                $result['status'] = 200;   
                $result['message'] = 'email sended';
            }
            else {
                $result['status'] = 300; 
            }   
        }
        else {
            $result['status'] = 300; 
        }

        return new JsonResponse($result);
    }

    /**
     * Recover broker Id by email
     * 
     * @param Request $request
     * @param Application $app
     * @return JsonResponse
    */
    public function forgetBrokerId(Request $request, Application $app) {

        $dataStorage = $app['data.storage.service'];
        $result = '';
        $brokerEmail = $request->get('brokerEmail');

        $broker = $dataStorage->getBrokerByEmail($brokerEmail);

        if ( $broker ) {
          
            // sending an email
            $emailBody = '';
            $emailBody .= 'Hello.<br>';
            $emailBody .= 'Your Broker ID: ' . $broker['id'];

            $result = $emailBody;

            // send an email
            $message = \Swift_Message::newInstance()
                ->setSubject('Quote link')
                ->setFrom(array('insurance@simonagency.com'))
                ->setTo([$broker['email']])
                ->setBody($emailBody)
                ->addPart(
                    $emailBody,
                    'text/html'
                );
            $app['mailer']->send($message);

            $result = 'email sended';
        }
        else {
            $result = 'no broker';
        }
        
        return new JsonResponse($result);
    }

    public function updateCobsWithState(Application $app) {
        echo "Start..." . PHP_EOL;

        try {
            echo "Initializing..." . PHP_EOL;
            $apiService = new \SimonApi\Service\HiscoxApi\HiscoxApigee($app['hiscox.apigee.config']);
            $dataAdapter = \SimonApi\Service\DataStorage\Adapter\AdapterFactory::getAdapter(
                $app['data.storage.driver'],
                $app['data.storage.config']);

            $dataStorage = $app['data.storage.service'];
            echo "done" . PHP_EOL;

            $allCobs = [];
            $states = $dataStorage->getStates();

            echo "Retrieving cobs list.." . PHP_EOL;
            foreach ($states as $state) {
                echo "State: ".$state['code'] . PHP_EOL;
                
                $cobList = $apiService->getCobListPartner($state['code']);
                echo count($cobList) . ' items' . PHP_EOL;

                $allCobs[$state['code']] = $cobList;
            }
               
            echo "Updating cobs list.." . PHP_EOL;
            $dataStorage->updateCobsWithState($allCobs);
            echo "done" . PHP_EOL;

        } catch (\Exception $e) {
            //TODO: add logging
            echo PHP_EOL . 'Failed! ' .
                 PHP_EOL . $e->getMessage() .
                 PHP_EOL . $e->getTraceAsString() .
                 PHP_EOL;
        }

        return new JsonResponse(true);
    }

    public function sendContactUs(Request $request, Application $app) {

        $result = [];
        $contact_fname = $request->get('contact_fname');
        $contact_lname = $request->get('contact_lname');
        $contact_email = $request->get('contact_email');
        $contact_phone = $request->get('contact_phone');
        $contact_comment = $request->get('contact_comment');
              
        // sending an email
        $emailBody = '';
        $emailBody .= 'Hello.<br><br>';
        $emailBody .= 'First Name: ' . $contact_fname . '<br>';
        $emailBody .= 'Last Name: ' . $contact_lname . '<br>';
        $emailBody .= 'Email: ' . $contact_email . '<br>';
        $emailBody .= 'Phone: ' . $contact_phone . '<br>';
        $emailBody .= 'Comment:<br>' . $contact_comment;

        // send an email
        $message = \Swift_Message::newInstance()
            ->setSubject('Contact Us')
            ->setFrom(array($contact_email))
            ->setTo($app['contactUsTo'])
            ->setBcc($app['contactUsCc'])
            ->setBody($emailBody)
            ->addPart(
                $emailBody,
                'text/html'
            );
        $app['mailer']->send($message);

        $result['status'] = 200;
        
        return new JsonResponse($result);
    }
}
