<?php

namespace SimonApi\Service\DataStorage\Adapter;

/**
 * Interface to database storage adapters
 */
interface AdapterInterface {
    /**
     * Get lists of available cobs
     *
     * @return array
     */
    public function getCobList();

    /**
     * Get lists of available cobs with state
     *
     * @return array
     */
    public function getCobByStateList($state);

    public function getTeamsList();

    public function getBrokerByEmail($email);
    public function getBrokerById($id);

    /**
     * Get list of available states
     *
     * @return array
     */
    public function getStateList();

    /**
     * Get list of eligible products
     *
     * @return array
     */
    public function getEligibleProductList();

    /**
     * Get list of eligible products link
     *
     * @return array
     */
    public function getEligibleProductLinkList();

    /**
     * Get eligible products by state and cob
     *
     * @param string $state
     * @param string $cob
     * @return array
     */
    public function getEligibleProductListByStateAndCob($state, $cob);

    /**
     * Update state
     *
     * @param array $stateData
     * @return bool
     */
    public function updateState($stateData);

    /**
     * Update list of states
     *
     * @param array $statesData
     * @return bool
     */
    public function updateStatesBulk($statesData);

    /**
     * Update cob
     *
     * @param array $cobData
     * @return bool
     */
    public function updateCob($cobData);

    /**
     * Update list of cobs
     *
     * @param array $cobsData
     * @return bool
     */
    public function updateCobsBulk($cobsData);

        /**
     * Update list of cobsWithState
     *
     * @param array $cobsData
     * @return bool
     */
    public function updateCobsWithStateBulk($cobsData);

    /**
     * Update list of eligible products
     *
     * @param array $eligibleList
     * @return mixed
     */
    public function updateEligibleProductsBulk($eligibleList);

    /**
     * @param array $linkArray
     * @return bool
     */
    public function updateEligibleProductLinks($linkArray);

    /**
     * @param array $brokerAppointment
     * @return bool
     */
    public function updateBrokerAppointment($brokerAppointment);
}
