<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.18
 * Time: 13:41
 */

namespace SimonApi\Service\DataStorage\Adapter;

/**
 * Adapter to posgres storage
 *
 * Class PostgresAdapter
 * @package SimonApi\Service\DataStorage\Adapter
 */
class PostgresAdapter extends AbstractAdapter
{

    private $connection = null;

    /**
     * PostgresAdapter constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->establishConnection($config);
    }

    /**
     * Establish connection to database
     *
     * @param $config
     */
    private function establishConnection($config) {
        $connectionString = '';

        static $connectionParamMap = [ 'host', 'port', 'dbname', 'user', 'password'];
        foreach ($config as $name => $value) {
            if (in_array($name, $connectionParamMap)) {
                $connectionString .= "{$name}={$value} ";
            }
        }
        $connectionString=trim($connectionString);

        if (empty($connectionString)) {
            throw new \BadMethodCallException('Incorrect config provided for postgres adapter');
        }

        $this->connection = pg_connect($connectionString);
        if (false === $this->connection) {
            throw new \RuntimeException('Cannot connect to postgres');
        }
    }

    /**
     * @inheritdoc
     */
    public function getCobList()
    {
        $query = "SELECT * FROM cobs";
        $result = pg_query($this->connection, $query);
        if ($result) {
            return pg_fetch_all($result);
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getCobByStateList($state)
    {
        $query = "SELECT name, displayname FROM cobs_with_state WHERE stateCode = '".$state."'";
        $result = pg_query($this->connection, $query);
        if ($result) {
            return pg_fetch_all($result);
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    public function getTeamsList()
    {
        $query = "SELECT t.*, ct.name as categoryName FROM teams t INNER JOIN categories_team ct ON t.categoryId = ct.Id";
        $result = pg_query($this->connection, $query);
        if ($result) {
            return pg_fetch_all($result);
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    public function getBrokerByEmail($email)
    {
        $query = "SELECT * FROM brokers WHERE email = '".$email."'";
        $result = pg_query($this->connection, $query);
        if ($result) {
            return pg_fetch_assoc($result);
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    public function getBrokerById($id)
    {
        $query = "SELECT * FROM brokers WHERE id = '".$id."'";
        $result = pg_query($this->connection, $query);
        if ($result) {
            return pg_fetch_assoc($result);
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getStateList()
    {
        $query = "SELECT * FROM states";
        $result = pg_query($this->connection, $query);
        if ($result) {
            $data = pg_fetch_all($result);
            return $data ? $data : [];
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductList()
    {
        $query = "SELECT * FROM eligible_products";
        $result = pg_query($this->connection, $query);
        if ($result) {
            $data = pg_fetch_all($result);
            return $data ? $data : [];
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductLinkList()
    {
        throw new \BadMethodCallException('Unsupported');
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductListByStateAndCob($state, $cob)
    {
        $query = "SELECT e.* FROM eligible_product_links l 
                  INNER JOIN eligible_products e ON e.id = l.eligible_product
                  WHERE l.state=$1 AND l.cob=$2";
        $result = pg_query_params($this->connection, $query, [$state, $cob]);
        if ($result) {
            $data = pg_fetch_all($result);
            return $data ? $data : [];
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function updateState($stateData)
    {
        // TODO: Implement updateState() method.
    }

    /**
     * @inheritdoc
     */
    public function updateCob($cobData)
    {
        // TODO: Implement updateCob() method.
    }

    /**
     * @inheritdoc
     */
    public function updateStatesBulk($statesData)
    {
        $query = 'INSERT INTO states (code, name) VALUES ';

        $queryData = [];
        foreach ($statesData as $stateData) {
            $queryData[] = '( ' .
                '\'' . pg_escape_string($this->connection, $stateData['code']) . '\',' .
                '\'' . pg_escape_string($this->connection, $stateData['name']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);
        $query .= 'ON CONFLICT (code) 
                    DO UPDATE 
                        SET name = excluded.name';


        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsBulk($cobsData)
    {
        $query = 'INSERT INTO cobs (code, name, displayName, friendlyName) VALUES ';

        $queryData = [];
        foreach ($cobsData as $cobData) {
            $queryData[] = '( ' .
                '\'' . pg_escape_string($this->connection, $cobData['code']) . '\',' .
                '\'' . pg_escape_string($this->connection, $cobData['name']) . '\',' .
                '\'' . pg_escape_string($this->connection, $cobData['displayName']) . '\',' .
                '\'' . pg_escape_string($this->connection, $cobData['friendlyName']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);
        $query .= 'ON CONFLICT (code) 
                    DO UPDATE 
                        SET name = excluded.name, 
                            displayName = excluded.displayName,
                            friendlyName = excluded.friendlyName';


        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsWithStateBulk($cobsData)
    {
        $query = 'INSERT INTO cobs_with_state (code, name, displayName, friendlyName, stateCode) VALUES ';

        $queryData = [];
        foreach ($cobsData as $key => $cobs) {
            foreach ($cobs as $cob) {
                $queryData[] = '( ' .
                    '\'' . pg_escape_string($this->connection, $cob['code']) . '\',' .
                    '\'' . pg_escape_string($this->connection, $cob['name']) . '\',' .
                    '\'' . pg_escape_string($this->connection, $cob['displayName']) . '\',' .
                    '\'' . pg_escape_string($this->connection, $cob['friendlyName']) . '\'' .
                    '\'' . pg_escape_string($this->connection, $key) . '\'' .
                    ') ';
            }
        }
        $query .= implode(',', $queryData);
        $query .= 'ON CONFLICT (code) 
                    DO UPDATE 
                        SET name = excluded.name, 
                            displayName = excluded.displayName,
                            friendlyName = excluded.friendlyName';


        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductsBulk($eligibleList)
    {
        $query = 'INSERT INTO eligible_products (id, name) VALUES ';

        $queryData = [];
        foreach ($eligibleList as $eligibleElement) {
            $queryData[] = '( ' .
                '\'' . pg_escape_string($this->connection, $eligibleElement['ID']) . '\',' .
                '\'' . pg_escape_string($this->connection, $eligibleElement['Name']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);
        $query .= 'ON CONFLICT (id) 
                    DO UPDATE 
                        SET name = excluded.name;';


        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductLinks($linkArray)
    {
        $query = 'TRUNCATE TABLE eligible_product_links';
        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }

        $query = 'INSERT INTO eligible_product_links (state, cob, eligible_product) VALUES ';

        $queryData = [];
        foreach ($linkArray as $linkElement) {
            $queryData[] = '( ' .
                '\'' . pg_escape_string($this->connection, $linkElement['state']) . '\',' .
                '\'' . pg_escape_string($this->connection, $linkElement['cob']) . '\',' .
                '\'' . pg_escape_string($this->connection, $linkElement['eligibleProduct']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateBrokerAppointment($brokerAppointment)
    {
        $query = 'INSERT INTO broker_appointments (first_name, last_name, company_name, email, telephone, mailing_address, city, business_interested, schedule_date, schedule_tz, license_file, license_name, eo_file, eo_name, w9_file, w9_name, agreement_file, agreement_name, company_folder) VALUES ';

        $query .= '( ' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['first_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['last_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['company_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['email']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['telephone']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['mailing_address']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['city']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['business_interested']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['schedule_date']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['schedule_tz']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['license_file']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['license_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['eo_file']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['eo_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['w9_file']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['w9_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['agreement_file']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['agreement_name']) . '\',' .
            '\'' . pg_escape_string($this->connection, $brokerAppointment['company_folder']) . '\'' .
        ') ';

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }


}
