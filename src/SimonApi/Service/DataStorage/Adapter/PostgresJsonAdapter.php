<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.18
 * Time: 13:41
 */

namespace SimonApi\Service\DataStorage\Adapter;

/**
 * Adapter to posgres storage
 *
 * Class PostgresAdapter
 * @package SimonApi\Service\DataStorage\Adapter
 */
class PostgresJsonAdapter extends AbstractAdapter
{
    const COBS_NAME = 'cob';
    const STATES_NAME = 'states';
    const ELIGIBLE_PRODUCTS_NAME = 'eligibleProducts';
    const ELIGIBLE_PRODUCT_LINKS_NAME = 'eligibleProductsLinks';
    const BROKER_APPOINTMENTS_NAME = 'brokerAppointments';
    const COBS_WITH_STATE_NAME = 'cobsWithState';
    const BROKERS_NAME = 'brokers';

    private $connection = null;

    /**
     * PostgresAdapter constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->establishConnection($config);
    }

    /**
     * Establish connection to database
     *
     * @param $config
     */
    private function establishConnection($config) {
        $connectionString = '';

        static $connectionParamMap = [ 'host', 'port', 'dbname', 'user', 'password'];
        foreach ($config as $name => $value) {
            if (in_array($name, $connectionParamMap)) {
                $connectionString .= "{$name}={$value} ";
            }
        }
        $connectionString=trim($connectionString);

        if (empty($connectionString)) {
            throw new \BadMethodCallException('Incorrect config provided for postgres adapter');
        }

        $this->connection = pg_connect($connectionString);
        if (false === $this->connection) {
            throw new \RuntimeException('Cannot connect to postgres');
        }
    }

    /**
     * @inheritdoc
     */
    public function getCobList()
    {
        $query = "SELECT data FROM api WHERE name='" . self::COBS_NAME . "'";
        $result = pg_query($this->connection, $query);
        if ($result && $row =  pg_fetch_assoc($result)) {
            $cobs = [];
            if ( !empty($row['data']) ) {
                $cobs = json_decode($row['data'], true);
                $cobs = $cobs['ClassesOfBusiness']['ClassOfBusiness'];
            }
            return $cobs;
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getCobByStateList($state)
    {
        $query = "SELECT data FROM api WHERE name='" . self::COBS_WITH_STATE_NAME . "'";
        $result = pg_query($this->connection, $query);
        if ($result && $row =  pg_fetch_assoc($result)) {
            $cobs = [];
            if ( !empty($row['data']) ) {
                $cobs = json_decode($row['data'], true);
                $cobs = $cobs['ClassesOfBusiness']['ClassOfBusiness'];
            }
            return $cobs;
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    public function getTeamsList()
    {
        return [];
    }


    public function getBrokerByEmail($email)
    {
        return [];
    }

    public function getBrokerById($id)
    {
        return [];
    }
  
    /**
     * @inheritdoc
     */
    public function getStateList()
    {
        $query = "SELECT data FROM api WHERE name='" . self::STATES_NAME . "'";
        $result = pg_query($this->connection, $query);
        if ($result && $row =  pg_fetch_assoc($result)) {
            $states = [];
            if ( !empty($row['data']) ) {
                $states = json_decode($row['data'], true);
                $states = $states['States']['State'];
            }
            return $states;
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductList()
    {
        $query = "SELECT data FROM api WHERE name='" . self::ELIGIBLE_PRODUCTS_NAME . "'";
        $result = pg_query($this->connection, $query);
        if ($result && $row =  pg_fetch_assoc($result)) {
            $eligibleProducts = [];
            if ( !empty($row['data']) ) {
                $eligibleProducts = json_decode($row['data'], true);
                $eligibleProducts = $eligibleProducts['EligibleProductsRs']['Products']['0']['Product'];
            }
            return $eligibleProducts;
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductLinkList()
    {
        $query = "SELECT data FROM api WHERE name='" . self::ELIGIBLE_PRODUCT_LINKS_NAME . "'";
        $result = pg_query($this->connection, $query);
        if ($result && $row =  pg_fetch_assoc($result)) {
            $links = [];
            if ( !empty($row['data']) ) {
                $links = json_decode($row['data'], true);
            }
            return $links;
        } else {
            throw new \RuntimeException('Error during postgres query: ' . pg_last_error($this->connection));
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductListByStateAndCob($state, $cob)
    {
        $result = [];
        //prepare eligible products hash
        $hash = [];
        $eligibleProducts = $this->getEligibleProductList();
        foreach ($eligibleProducts as $eligibleProduct) {
            $hash[$eligibleProduct['ID']] = $eligibleProduct;
        }
        $searchTree = $this->getEligibleProductLinkList();
        if(!empty($searchTree[$state][$cob])) {
            foreach ($searchTree[$state][$cob] as $code) {
                array_push($result, $hash[$code]);
            };
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function updateState($stateData)
    {
        throw new \BadMethodCallException('Unsupported');
    }

    /**
     * @inheritdoc
     */
    public function updateCob($cobData)
    {
        throw new \BadMethodCallException('Unsupported');
    }

    /**
     * @inheritdoc
     */
    public function updateStatesBulk($statesData)
    {
        $statesObject = [];
        $statesObject['States']['State'] = $statesData;
        $statesObjectJson = json_encode($statesObject);
        $query = "UPDATE api SET data='$statesObjectJson' WHERE name = '" . self::STATES_NAME . "'";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsBulk($cobsData)
    {
        $cobsObject = [];
        $cobsObject['ClassesOfBusiness']['ClassOfBusiness'] = $cobsData;
        $cobsObjectJson = json_encode($cobsObject);
        $query = "UPDATE api SET data='$cobsObjectJson' WHERE name = '" . self::COBS_NAME . "'";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsWithStateBulk($cobsData)
    {
        $cobsObjectJson = json_encode($cobsData);
        $query = "UPDATE api SET data='$cobsObjectJson' WHERE name = '" . self::COBS_NAME . "'";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductsBulk($eligibleList)
    {

        $query = "DELETE FROM api WHERE name= '" . self::ELIGIBLE_PRODUCTS_NAME . "'";
        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }

        $eligibleProductObject = [];
        $eligibleProductObject['EligibleProductsRs']['Products']['0']['Product'] = array_values($eligibleList);
        $eligibleProductJson = pg_escape_string(json_encode($eligibleProductObject));
        $query = "INSERT INTO api (name, data) VALUES ('" . self::ELIGIBLE_PRODUCTS_NAME . "', '$eligibleProductJson')";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductLinks($linkArray)
    {
        $query = "DELETE FROM api WHERE name= '" . self::ELIGIBLE_PRODUCT_LINKS_NAME . "'";
        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }

        $eligibleProductLinksObject = $this->transformDataToTree($linkArray);
        $eligibleProductLinkJson = pg_escape_string(json_encode($eligibleProductLinksObject));
        $query = "INSERT INTO api (name, data) VALUES ('" . self::ELIGIBLE_PRODUCT_LINKS_NAME . "', '$eligibleProductLinkJson')";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }

    /**
     * Transforms data to easy search state -> cob -> codes
     *
     * @param array $linkArray
     * @return array
     */
    private function transformDataToTree($linkArray) {
        $result = [];
        foreach ($linkArray as $linkElement) {
            if (!isset($result[$linkElement['state']])) {
                $result[$linkElement['state']] = [];
            }
            if (!isset($result[$linkElement['state']][$linkElement['cob']])) {
                $result[$linkElement['state']][$linkElement['cob']] = [];
            }
            array_push($result[$linkElement['state']][$linkElement['cob']], $linkElement['eligibleProduct']);

        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function updateBrokerAppointment($brokerAppointment)
    {
        $brokerAppointmentObject = $this->transformDataToTree($brokerAppointment);
        $brokerAppointmentJson = pg_escape_string(json_encode($brokerAppointmentObject));
        $query = "INSERT INTO api (name, data) VALUES ('" . self::BROKER_APPOINTMENTS_NAME . "', '$brokerAppointmentJson')";

        $result = pg_query($this->connection, $query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . pg_last_error($this->connection));
        }
        return true;
    }
}