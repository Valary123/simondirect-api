<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.04.18
 * Time: 13:57
 */

namespace SimonApi\Service\DataStorage\Adapter;


/**
 * Adapter to mysql storage
 *
 * Class PostgresAdapter
 * @package SimonApi\Service\DataStorage\Adapter
 */
class MysqlAdapter extends AbstractAdapter
{

    /**
     * @var \Mysqli
     */
    private $connection = null;

    /**
     * MysqlAdapter constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->establishConnection($config);
    }

    /**
     * Establish connection to database
     *
     * @param $config
     */
    private function establishConnection($config) {
        $this->connection = new \Mysqli(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['dbname']
        );

        if ($this->connection->connect_errno) {
            throw new \RuntimeException(
                "Cannot connect to mysql ("
                . $this->connection->connect_errno . "): "
                . $this->connection->connect_error
            );
        }

    }

    /**
     * @inheritdoc
     */
    public function getCobList()
    {
        if ($dbResult = $this->connection->query("SELECT * FROM cobs")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function getCobByStateList($state)
    {
        if ($dbResult = $this->connection->query("SELECT name, displayname FROM cobs_with_state WHERE stateCode = '".$state."'")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    public function getTeamsList()
    {
        if ($dbResult = $this->connection->query("SELECT t.*, ct.name as categoryName FROM teams t INNER JOIN categories_team ct ON t.categoryId = ct.Id")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    public function getBrokerByEmail($email)
    {
        if ($dbResult = $this->connection->query("SELECT * FROM brokers WHERE email = '".$email."'")) {
            return $dbResult->fetch_assoc();
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    public function getBrokerById($id)
    {
        if ($dbResult = $this->connection->query("SELECT * FROM brokers WHERE id = '".$id."'")) {
            return $dbResult->fetch_assoc();
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function getStateList()
    {
        if ($dbResult = $this->connection->query("SELECT * FROM states")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductList()
    {
        if ($dbResult = $this->connection->query("SELECT * FROM eligible_products")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductLinkList()
    {
        if ($dbResult = $this->connection->query("SELECT * FROM eligible_product_links")) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $this->transformDataToTree($result);
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function getEligibleProductListByStateAndCob($state, $cob)
    {
        $stateEsc = $this->connection->real_escape_string($state);
        $cobEsc = $this->connection->real_escape_string($cob);
        $query = "SELECT e.* FROM eligible_product_links l 
                  INNER JOIN eligible_products e ON e.id = l.eligible_product
                  WHERE l.state='{$stateEsc}' AND l.cob='{$cobEsc}'";
        if ($dbResult = $this->connection->query($query)) {
            $result = [];
            while ($row = $dbResult->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            throw new \RuntimeException("Mysql query error: " . $this->connection->error);
        }
    }

    /**
     * @inheritdoc
     */
    public function updateState($stateData)
    {
        // TODO: Implement updateState() method.
    }

    /**
     * @inheritdoc
     */
    public function updateCob($cobData)
    {
        // TODO: Implement updateCob() method.
    }

    /**
     * @inheritdoc
     */
    public function updateStatesBulk($statesData)
    {
        $query = 'TRUNCATE TABLE states';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'INSERT INTO states (code, name) VALUES ';

        $queryData = [];
        foreach ($statesData as $stateData) {
            $queryData[] = '( ' .
                '\'' . $this->connection->real_escape_string($stateData['code']) . '\',' .
                '\'' . $this->connection->real_escape_string($stateData['name']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsBulk($cobsData)
    {
        $query = 'TRUNCATE TABLE cobs';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'INSERT INTO cobs (code, name, displayName, friendlyName) VALUES ';

        $queryData = [];
        foreach ($cobsData as $cobData) {
            $queryData[] = '( ' .
                '\'' . $this->connection->real_escape_string($cobData['code']) . '\',' .
                '\'' . $this->connection->real_escape_string($cobData['name']) . '\',' .
                '\'' . $this->connection->real_escape_string($cobData['displayName']) . '\',' .
                '\'' . $this->connection->real_escape_string($cobData['friendlyName']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateCobsWithStateBulk($cobsData)
    {
        $query = 'TRUNCATE TABLE cobs_with_state';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'ALTER TABLE cobs_with_state AUTO_INCREMENT = 1';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'INSERT INTO cobs_with_state (code, name, displayName, friendlyName, stateCode) VALUES ';

        $queryData = [];
        foreach ($cobsData as $key => $cobs) {
            foreach ($cobs as $cob) {
                $queryData[] = '( ' .
                    '\'' . $this->connection->real_escape_string($cob['code']) . '\',' .
                    '\'' . $this->connection->real_escape_string($cob['name']) . '\',' .
                    '\'' . $this->connection->real_escape_string($cob['displayName']) . '\',' .
                    '\'' . $this->connection->real_escape_string($cob['friendlyName']) . '\',' .
                    '\'' . $this->connection->real_escape_string($key) . '\'' .
                    ') ';
            }
        }
        $query .= implode(',', $queryData);

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductsBulk($eligibleList)
    {
        $query = 'TRUNCATE TABLE eligible_products';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'INSERT INTO eligible_products (id, name) VALUES ';

        $queryData = [];
        foreach ($eligibleList as $eligibleElement) {
            $queryData[] = '( ' .
                '\'' . $this->connection->real_escape_string($eligibleElement['ID']) . '\',' .
                '\'' . $this->connection->real_escape_string($eligibleElement['Name']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateEligibleProductLinks($linkArray)
    {
        $query = 'TRUNCATE TABLE eligible_product_links';
        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }

        $query = 'INSERT INTO eligible_product_links (state, cob, eligible_product) VALUES ';

        $queryData = [];
        foreach ($linkArray as $linkElement) {
            $queryData[] = '( ' .
                '\'' . $this->connection->real_escape_string($linkElement['state']) . '\',' .
                '\'' . $this->connection->real_escape_string($linkElement['cob']) . '\',' .
                '\'' . $this->connection->real_escape_string($linkElement['eligibleProduct']) . '\'' .
                ') ';
        }
        $query .= implode(',', $queryData);

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

    /**
     * Transforms data to easy search state -> cob -> codes
     *
     * @param array $linkArray
     * @return array
     */
    private function transformDataToTree($linkArray) {
        $result = [];
        foreach ($linkArray as $linkElement) {
            if (!isset($result[$linkElement['state']])) {
                $result[$linkElement['state']] = [];
            }
            if (!isset($result[$linkElement['state']][$linkElement['cob']])) {
                $result[$linkElement['state']][$linkElement['cob']] = [];
            }
            array_push($result[$linkElement['state']][$linkElement['cob']], $linkElement['eligible_product']);

        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function updateBrokerAppointment($brokerAppointment)
    {
        $query = 'INSERT INTO broker_appointments (first_name, last_name, company_name, email, telephone, mailing_address, city, business_interested, schedule_date, schedule_tz, license_file, license_name, eo_file, eo_name, w9_file, w9_name, agreement_file, agreement_name, company_folder) VALUES ';

        $query .= '( ' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['first_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['last_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['company_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['email']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['telephone']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['mailing_address']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['city']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['business_interested']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['schedule_date']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['schedule_tz']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['license_file']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['license_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['eo_file']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['eo_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['w9_file']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['w9_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['agreement_file']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['agreement_name']) . '\',' .
            '\'' . $this->connection->real_escape_string($brokerAppointment['company_folder']) . '\'' .
        ') ';

        $result = $this->connection->query($query);
        if (false === $result) {
            throw new \RuntimeException('Error executing query: ' . $this->connection->error);
        }
        return true;
    }

}
