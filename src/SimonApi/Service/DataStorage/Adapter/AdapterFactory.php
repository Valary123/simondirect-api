<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.18
 * Time: 14:16
 */

namespace SimonApi\Service\DataStorage\Adapter;

/**
 * Class AdapterFactory
 * @package SimonApi\Service\DataStorage\Adapter
 */
class AdapterFactory
{
    const POSTGRES_ADAPTER = 'postgres';
    const POSTGRES_JSON_ADAPTER = 'postgres_json';
    const MYSQL_ADAPTER = 'mysql';

    /**
     * Get adapter by name
     *
     * @param string $name
     * @param array $config
     * @return AdapterInterface
     */
    public static function getAdapter($name, array $config) {
        if (!isset($config[$name])) {
            throw new \BadMethodCallException("No config for adapter {$name}");
        }
        $specifiedConfig = $config[$name];
        switch ($name) {
            case self::POSTGRES_ADAPTER:
                return new PostgresAdapter($specifiedConfig);
            case self::POSTGRES_JSON_ADAPTER:
                return new PostgresJsonAdapter($specifiedConfig);
            case self::MYSQL_ADAPTER:
                return new MysqlAdapter($specifiedConfig);
            default:
                throw new \BadMethodCallException("Unknown adapter {$name}");
        }
    }
}
