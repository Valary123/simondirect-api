<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.18
 * Time: 13:38
 */

namespace SimonApi\Service\DataStorage\Adapter;

/**
 * Base adapter implementation
 *
 * Class AbstractAdapter
 * @package SimonApi\Service\DataStorage\Adapter
 */
abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * @inheritdoc
     */
    abstract public function getCobList();

    /**
     * @inheritdoc
     */
    abstract public function getCobByStateList($state);

    /**
     * @inheritdoc
     */
    abstract public function getTeamsList();

    /**
     * @inheritdoc
     */
    abstract public function getStateList();

    /**
     * @inheritdoc
     */
    abstract public function getEligibleProductList();

    /**
     * @inheritdoc
     */
    abstract public function getEligibleProductLinkList();

    /**
     * @inheritdoc
     */
    abstract public function getEligibleProductListByStateAndCob($state, $cob);

    /**
     * @inheritdoc
     */
    abstract public function getBrokerByEmail($email);
    abstract public function getBrokerById($id);

    /**
     * @inheritdoc
     */
    abstract public function updateState($stateData);

    /**
     * @inheritdoc
     */
    public function updateStatesBulk($statesData)
    {
        foreach ($statesData as $stateData) {
            $this->updateState($stateData);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    abstract public function updateCob($cobData);

    /**
     * @inheritdoc
     */
    public function updateCobsBulk($cobsData)
    {
        foreach ($cobsData as $cobData) {
            $this->updateState($cobData);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    abstract public function updateCobsWithStateBulk($cobData);

    /**
     * @inheritdoc
     */
    abstract public function updateEligibleProductsBulk($eligibleList);

    /**
     * @inheritdoc
     */
    abstract public function updateEligibleProductLinks($linkArray);

    /**
     * @inheritdoc
     */
    abstract public function updateBrokerAppointment($brokerAppointment);

}
