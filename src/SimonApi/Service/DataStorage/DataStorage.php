<?php

namespace SimonApi\Service\DataStorage;

use SimonApi\Service\DataStorage\Adapter\AdapterInterface;

/**
 * Class DataStorage
 * @package SimonApi\Service\DataStorage
 */
class DataStorage
{

    /**
     * @var AdapterInterface
     */
    private $adapter = null;

    /**
     * DataStorage constructor.
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Retrieve cobs from storage
     *
     * @return array
     */
    public function getCobs() {
        return $this->adapter->getCobList();
    }

    public function getTeams() {
        return $this->adapter->getTeamsList();
    }

    /**
     * Retrieve cobs_with_state from storage
     *
     * @return array
     */
    public function getCobsByState($state) {
        return $this->adapter->getCobByStateList($state);
    }

    public function getBrokerByEmail($email) {
        return $this->adapter->getBrokerByEmail($email);
    }

    public function getBrokerById($id) {
        return $this->adapter->getBrokerById($id);
    }

    /**
     * Retrieve states from storage
     *
     * @return array
     */
    public function getStates() {
        return $this->adapter->getStateList();
    }

    /**
     * Get eligible product list
     *
     * @return array
     */
    public function getEligibleProducts() {
        return $this->adapter->getEligibleProductList();
    }

    /**
     * Get eligible product list links
     *
     * @return array
     */
    public function getEligibleProductLinkList() {
        return $this->adapter->getEligibleProductLinkList();
    }

    /**
     * Get eligible products for state and cob
     *
     * @param string $state
     * @param string $cob
     * @return array
     */
    public function getEligibleProductsByStateAndCob($state, $cob) {
        return $this->adapter->getEligibleProductListByStateAndCob($state, $cob);
    }

    /**
     * Update cobs in database
     *
     * @param array $cobsData
     * @return bool
     */
    public function updateCobs($cobsData) {
        return $this->adapter->updateCobsBulk($cobsData);
    }

    /**
     * Update cobsWithState in database
     *
     * @param array $cobsData
     * @return bool
     */
    public function updateCobsWithState($cobsData) {
        return $this->adapter->updateCobsWithStateBulk($cobsData);
    }

    /**
     * Update states in database
     *
     * @param array $statesData
     * @return bool
     */
    public function updateStates($statesData) {
        return $this->adapter->updateStatesBulk($statesData);
    }

    /**
     * Update eligible product list
     *
     * @param array $eligibleProducts
     * @return bool
     */
    public function updateEligibleProducts($eligibleProducts) {
        return $this->adapter->updateEligibleProductsBulk($eligibleProducts);
    }

    /**
     * Update links between eligible products and states and cobs
     *
     * @param array $eligibleProductLinks
     * @return bool
     */
    public function updateEligibleProductLinks($eligibleProductLinks) {
        return $this->adapter->updateEligibleProductLinks($eligibleProductLinks);
    }

    /**
     * Update broker appointment
     *
     * @param array $brokerAppointment
     * @return bool
     */
    public function updateBrokerAppointment($brokerAppointment) {
        return $this->adapter->updateBrokerAppointment($brokerAppointment);
    }
}