<?php

namespace SimonApi\Service\HiscoxApi;

use Psr\Log\LoggerInterface;

/**
 * Class HiscoxApi.
 * @package SimonApi\Service\HiscoxApi
 */
class HiscoxApi
{

    /**
     * @var string Authentication token
     */
    private $accessToken          = null;

    /**
     * @var string Endpoint to api requests
     */
    private $apiUrl         = null;

    /**
     * @var string Consumer login (ClientId)
     */
    private $consumerKey    = null;

    /**
     * @var string Consumer secret
     */
    private $consumerSecret = null;

    /**
     * @var LoggerInterface
     */
    private $logger = null;

    /**
     * HiscoxApi constructor.
     * @param LoggerInterface $logger
     * @param array $config
     */
    public function __construct($config, LoggerInterface $logger = null) {
        $this->apiUrl           = $config['api_url'];
        $this->consumerKey      = $config['consumer_key'];
        $this->consumerSecret   = $config['consumer_secret'];
        $this->logger           = $logger;
    }

    /**
     * Get access token and login if unavailable
     *
     * @return string
     */
    private function getAccessToken() {
        if (null === $this->accessToken) {
            $this->loginToApi();
        }
        return $this->accessToken;
    }

    /**
     * Perform login to api
     */
    private function loginToApi() {
        $bearerToken = base64_encode($this->consumerKey . ':' . $this->consumerSecret);
        $bearerTokenAsHeader = 'Authorization: ' . $bearerToken;
        $result = $this->makeSingleCurlRequest('/toolbox/auth/accesstoken', [$bearerTokenAsHeader]);
        $this->accessToken = $result['access_token'];
    }

    /**
     * Perform request to specified endpoint and returns parsed object
     *
     * @param string $endpoint
     * @param array $headers
     * @param array $params
     * @return array
     */
    private function makeSingleCurlRequest($endpoint, $headers = [], $params = []) {

        try {
            $response = null;
            $ch = $this->prepareCurlRequest($endpoint, $headers, $params);
            $response = curl_exec($ch);

            $result = $this->processCurlResult($ch, $response);
            curl_close($ch);
            return $result;
        } catch (\RuntimeException $e) {
            if ($this->logger) {
                $this->logger->warning($e->getMessage());
            }
            throw $e;
        }
    }

    /**
     * Perform async array of requests
     *
     * @param array $endpoints
     * @param array $headers
     * @param array $params
     * @return array
     */
    private function makeMultipleCurlRequest($endpoints, $headers = [], $params = []) {

        $mh = curl_multi_init();
        $handlesArray = [];
        foreach ($endpoints as $id => $endpoint) {
            $ch = $this->prepareCurlRequest($endpoint, $headers, $params);
            $handlesArray[$id] = $ch;
            curl_multi_add_handle($mh, $ch);
        }

        do {
            curl_multi_exec($mh, $running);
            curl_multi_select($mh);
        } while ($running > 0);

        $resArray = [];
        foreach ($handlesArray as $id => $ch) {
            $response = curl_multi_getcontent($ch);
            $resArray[$id] = $this->processCurlResult($ch, $response);
            curl_multi_remove_handle($mh, $ch);
        }
        curl_multi_close($mh);
        return $resArray;
    }

    /**
     * Prepare curl request
     *
     * @param string $endpoint
     * @param array $headers
     * @param array $params
     * @return resource
     */
    private function prepareCurlRequest($endpoint, array $headers = [], array $params = []) {
        $url = $this->apiUrl . $endpoint;
        $baseHeaders = ['Accept: application/json', 'Content-Type: application/json'];
        $commonHeaders = array_merge($baseHeaders, $headers);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $commonHeaders);

        // post fields
        if ( !empty($params) ) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        return $ch;
    }

    /**
     * Perform curl result processing
     *
     * @param $ch
     * @param $response
     * @return mixed
     */
    private function processCurlResult($ch, $response) {
        try {
            // request failed
            if ($response === false) {
                throw new \RuntimeException('Error making api request: ' . curl_error($ch));
            }
            // response != 200 code
            $info = curl_getinfo($ch);
            if ( $info['http_code'] != 200){
                var_dump($info['url']);
                throw new \RuntimeException('Wrong code received from api: ' . $info['http_code']);
            }
            $responseObj = json_decode($response, true);
            if (null === $responseObj) {
                throw new \RuntimeException('Wrong server response: ' . $response);
            }
            if (isset($responseObj['Error'])) {
                throw new \RuntimeException('API error response: ' . $responseObj['Error']);
            }
            return $responseObj;
        } catch (\RuntimeException $e) {
            if ($this->logger) {
                $this->logger->warning($e->getMessage());
            }
            throw $e;
        }
    }

    /**
     * Retrieve cobs list
     *
     * @return mixed
     */
    public function getCobList() {
        $accessToken = $this->getAccessToken();
        $accessTokenAsHeader = 'Authorization: Bearer ' . $accessToken;
        $result = $this->makeSingleCurlRequest('/toolbox/v1/cobs', [$accessTokenAsHeader]);
        return $result['ClassesOfBusiness']['ClassOfBusiness'];
    }

    /**
     * Retrieve states list
     *
     * @return mixed
     */
    public function getStatesList() {
        $accessToken = $this->getAccessToken();
        $accessTokenAsHeader = 'Authorization: Bearer ' . $accessToken;
        $result = $this->makeSingleCurlRequest('/toolbox/v1/states', [$accessTokenAsHeader]);
        return $result['States']['State'];
    }

    /**
     * Retrieve eligible products for state and cob
     *
     * @param $state
     * @param $cob
     * @return mixed
     */
    public function getEligibleProducts($state, $cob) {
        $accessToken = $this->getAccessToken();
        $accessTokenAsHeader = 'Authorization: Bearer ' . $accessToken;
        $result = $this->makeSingleCurlRequest("/partner/v1/eligibleproducts/{$state}/{$cob}", [$accessTokenAsHeader]);
        if (isset($result['EligibleProductsRs']['Products']['0']['Product'])){
            return $result['EligibleProductsRs']['Products']['0']['Product'];
        } else {
            // API returns NoEligibleProducts
            return [];
        }
    }

    /**
     * Retrieve multiple array of eligible products
     *
     * @param string $cob
     * @param array $states
     * @return array
     */
    public function getEligibleProductsForCob($cob, array $states) {
        $accessToken = $this->getAccessToken();
        $accessTokenAsHeader = 'Authorization: Bearer ' . $accessToken;
        $endpoints = [];
        foreach ($states as $state) {
            $endpoints[$state] = "/partner/v1/eligibleproducts/{$state}/{$cob}";
        }
        $resultArray = $this->makeMultipleCurlRequest($endpoints, [$accessTokenAsHeader]);
        foreach ($resultArray as &$result) {
            if (isset($result['EligibleProductsRs']['Products']['0']['Product'])){
                $result = $result['EligibleProductsRs']['Products']['0']['Product'];
            } else {
                // API returns NoEligibleProducts
                $result = [];
            }
        }
        return $resultArray;
    }
}
