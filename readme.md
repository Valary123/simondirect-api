# Api service #
 

## System requirements ##

  * PHP 7.1 with
     * php-curl
     * php-pgsql
  * PostgreSQL database
  * Composer 

## Installation guide ##

#### Clone project ####

`$ git clone git@bitbucket.org:gmsorrow/simondirect-api.git`

#### Install project requirements ####
`$ cd simondirect-api && composer install`

#### Configure web server ####

You need to configure your web server to {path_to_project}/public directory 
as a root. Below nginx configuration example:
```
server {
    server_name url.localhost;
    root {path_to_project}/public;

    location / {
        # try to serve file directly, fallback to front controller
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }
}
```
Apache .htaccess file is included. Virtual host configuration example as following:
```
<VirtualHost *:80>
    ServerName simondirect-api.localhost
    ServerAdmin webmaster@pc77-ubuntu-server
    DocumentRoot {path_to_project}/public

    ErrorLog ${APACHE_LOG_DIR}/simondirect-api-error.log
    CustomLog ${APACHE_LOG_DIR}/simondirect-api-access.log combined
    <Directory />
        Require all granted
        AllowOverride all
    </Directory>
</VirtualHost>

```

#### Configure application ####
{path_to_project}/app/config directory contains config.php.dist file. Copy this file to config.php and fulfil.
Add {path_to_project}/bin/update script to crontab list to schedule database update.

